import 'package:flip_card/flip_card.dart';
import 'package:flip_card/flip_card_controller.dart';
import 'package:simple_grid/simple_grid.dart';
import 'package:flutter/material.dart';
import '/component/menu_main.dart';
import 'Page.dart';

BuildContext? oldContext;

class Planning extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // toolbarHeight: 100,
        backgroundColor: Colors.brown,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.menu,
                color: Color.fromARGB(255, 0, 0, 0),
                // size: 40, // Changing Drawer Icon Size
              ),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: "เมนู",
            );
          },
        ),
        title: Text(
          "วิชาที่ลงทะเบียน",
          style: TextStyle(color: Colors.black),
        ),
      ),

      body: ListView(
        children: [
          SpGrid(
            children: [
              SpGridItem(
                xs: 12,
                md: 12,
                order: SpOrder(sm: 1, xs: 1),
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                  child: Center(
                      child: Text(
                    "รายวิชาที่ลงทะเบียน",
                    style: TextStyle(fontSize: 22),
                  )),
                ),

              ),

              SpGridItem(
                xs: 12,
                sm: 6,
                md: 4,
                lg: 3,
                child: Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: buildCard(
                        "Web Programming",
                        "การเขียนโปรแกรมบนเว็บ",
                        context)),
              ),
              SpGridItem(
                xs: 12,
                sm: 6,
                md: 4,
                lg: 3,
                order: SpOrder(sm: 1, xs: 1),
                child: Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: buildCard(
                        "Object-Oriented Analysis and Design",
                        "การวิเคราะห์และออกแบบเชิงวัตถุ",
                        context)),
              ),
              SpGridItem(
                xs: 12,
                sm: 6,
                md: 4,
                lg: 3,
                order: SpOrder(sm: 1, xs: 1),
                child: Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: buildCard(
                        "Mobile Application Development I",
                        "การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1",
                        context)),
              ),
              SpGridItem(
                xs: 12,
                sm: 6,
                md: 4,
                lg: 3,
                order: SpOrder(sm: 1, xs: 1),
                child: Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: buildCard(
                        "Software Testing",
                        "การทดสอบซอฟต์แวร์",
                        context)),
              ),
              SpGridItem(
                xs: 12,
                sm: 6,
                md: 4,
                lg: 3,
                order: SpOrder(sm: 1, xs: 1),
                child: Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: buildCard(
                        "Multimedia Programming for Multiplatforms",
                        "การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม",
                        context)),
              ),
              SpGridItem(
                xs: 12,
                sm: 6,
                md: 4,
                lg: 3,
                order: SpOrder(sm: 1, xs: 1),
                child: Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: buildCard(
                        "Computer Networks",
                        "เครือข่ายคอมพิวเตอร์",
                        context)),
              ),
              SpGridItem(
                xs: 12,
                sm: 6,
                md: 4,
                lg: 3,
                order: SpOrder(sm: 1, xs: 1),
                child: Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: buildCard(
                        "Probability and Statistics for Computing",
                        "ความน่าจะเป็นและสถิติสำหรับคอมพิวเตอร์",
                        context)),
              ),
              SpGridItem(
                xs: 12,
                sm: 6,
                md: 4,
                lg: 3,
                order: SpOrder(sm: 1, xs: 1),
                child: Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: buildCard(
                        "Relational Database",
                        "ฐานข้อมูลเชิงสัมพันธ์",
                        context)),
              ),
            ],
          ),
        ],
      ),
      drawer: MyDrawer(),
    );
  }
}

FlipCard buildCard(

    String finalExam,
    String code,
    BuildContext context) {
  GlobalKey<FlipCardState> cardKey = GlobalKey<FlipCardState>();

  return FlipCard(
    flipOnTouch: false,
    key: cardKey,
    fill: Fill.fillBack,
    direction: FlipDirection.HORIZONTAL, // default
    front: Container(
      // required
      child: Card(
        elevation: 4.0,
        child: Column(
          children: [
            ListTile(
              title: Text(finalExam),
              subtitle: Text(code),
            ),
          ],
        ),
      ),
    ),

    back: Card(
      elevation: 10.0,
      child: ListView(
        children: [
          ListTile(
            title: Text(finalExam),
            subtitle: Text(code),
          ),
          // Divider(
          //   height: 5,
          // ),
        ],
      ),
    ),
  );
}
