import 'package:flutter/material.dart';
import 'package:weather/Personor.dart';
import 'package:weather_icons/weather_icons.dart';
import 'package:device_preview/device_preview.dart';
import 'package:marquee/marquee.dart';
import 'Page.dart';
import 'login.dart';
import 'timetable_page.dart';

Text textDegree = Text(
  "\u2103",
  style: TextStyle(fontSize: 20),
);
void main() {
  runApp(weatherApp());
}

class weatherApp extends StatefulWidget {
  @override
  State<weatherApp> createState() => _weatherAppState();
}

class _weatherAppState extends State<weatherApp> {
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'MyReg',
        theme: ThemeData(
          primarySwatch: Colors.yellow,
        ),
        home: Login(),
      ),
    );
  }
}
