import 'package:simple_grid/simple_grid.dart';
import 'package:flutter/material.dart';
import 'package:weather/component/menu_main.dart';
import 'package:at_gauges/at_gauges.dart';

class History extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // toolbarHeight: 100,
        backgroundColor: Colors.brown,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.menu,
                color: Color.fromARGB(255, 0, 0, 0),
                // size: 40, // Changing Drawer Icon Size
              ),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: "เมนู",
            );
          },
        ),
        title: Text(
          "ประวัตินิสิต",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: ListView(
        children: [
          SpGrid(
            alignment: WrapAlignment.center,
            width: MediaQuery.of(context).size.width,
            spacing: 10,
            runSpacing: 10,
            children: [
              SpGridItem(
                xs: 12,
                md: 12,
                lg: 12,
                order: SpOrder(sm: 1, xs: 1, lg: 0),
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                  child: Center(
                      ),
                ),
              ),
              SpGridItem(
                xs: 12,
                md: 12,
                lg: 12,
                order: SpOrder(sm: 1, xs: 1, lg: 0),
                child: Container(
                  
                  width: 300,
                  height: 300,
                  child: Center(
                    child: Image.network(
                      "https://media.discordapp.net/attachments/971027009549508638/1069231337942306846/292286513_5217462678299354_4849782763499329947_n.png?width=671&height=671",
                      fit: BoxFit.cover,
                    ),
                  ),

                ),
              ),

              SpGridItem(
                xs: 12,
                md: 12,
                lg: 12,
                order: SpOrder(sm: 1, xs: 1),
                child: Center(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Container(
                      // color: Colors.teal,

                      child: DataTable(
                        columnSpacing: 1.0,
                        columns: [
                          DataColumn(
                              label: Text('ข้อมูลนิสิต',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold))),
                          DataColumn(
                              label: Text('',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold))),
                        ],
                        rows: [
                          DataRow(cells: [
                            DataCell(Text('รหัสประจำตัว:')),
                            DataCell(Text('63160199')),
                          ]),
                          DataRow(cells: [
                            DataCell(Text('ชื่อ:')),
                            DataCell(Text('นายธนาดล โหระกุล')),
                          ]),
                          DataRow(cells: [
                            DataCell(Text('เบอร์โทร:')),
                            DataCell(Text('0914493708')),
                          ]),
                          DataRow(cells: [
                            DataCell(Text('เบอร์โทรผู้ปกครอง:')),
                            DataCell(Text('0818593650')),
                          ]),
                          DataRow(cells: [
                            DataCell(Text('ที่อยู่:')),
                            DataCell(Text('ราชบุรี')),
                          ]),
                          DataRow(cells: [
                            DataCell(Text('นิสิตปีที่:')),
                            DataCell(Text('3')),
                          ]),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      drawer: MyDrawer(),
    );
  }
}
