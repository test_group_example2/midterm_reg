import 'package:flutter/material.dart';
import 'package:weather/Personor.dart';
import 'package:weather/Page.dart';
import 'package:weather/timetable_page.dart';
import 'package:weather/login.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const UserAccountsDrawerHeader(
            decoration: BoxDecoration(color: Colors.brown),
            accountName: Text(
              "Tanadon Horakul",
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
            ),
            accountEmail: Text(
              "63160199@go.buu.ac.th",
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
            ),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage(
                'assets/image/pro.png',
              ),
            ),
          ),

          ListTile(
            leading: Icon(
              Icons.home,
            ),
            title: const Text('หน้าแรก'),
            // subtitle: Text("ย้อนกลับไปยังหน้าแรก"),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => const HomePage()),
              );
            },
          ),
          ListTile(
            leading: Icon(
              Icons.menu_book_sharp,
            ),
            title: const Text('ตารางเรียน/สอบ'),
            // subtitle: Text("ดูวันเวลาในการสอบ"),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => Planning()),
              );
              // Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(
              Icons.man_sharp,
            ),
            title: const Text('ประวัตินิสิต'),
            // subtitle: Text("ตรวจสอบประวัติของตนเอง"),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => History()),
              );
            },
          ),

          ListTile(
            leading: Icon(
              Icons.logout,
            ),
            title: const Text('ออกจากระบบ'),
            // subtitle: Text("ออกจากระบบ REG"),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => Login()),
              );
            },
          ),
          // AboutListTile(
          //   // <-- SEE HERE
          //   icon: Icon(
          //     Icons.info,
          //   ),
          //   child: Text('About app'),
          //   applicationIcon: Icon(
          //     Icons.local_play,
          //   ),
          //   applicationName: 'My Cool App',
          //   applicationVersion: '1.0.25',
          //   applicationLegalese: '© 2019 Company',
          //   aboutBoxChildren: [
          //     ///Content goes here...
          //   ],
          // ),
        ],
      ),
    );
  }
}
