import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:marquee/marquee.dart';
import 'package:weather/login.dart';
import 'timetable_page.dart';
import 'package:simple_grid/simple_grid.dart';
import 'component/menu_main.dart';
import 'package:badges/badges.dart' as badges;

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}


class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.menu,
                color: Colors.black,
                // size: 40, // Changing Drawer Icon Size
              ),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: 'เมนู',
            );
          },
        ),
        title: Container(
            child: Row(
          children: [
            Column(
              children: [
                Container(
                  margin: const EdgeInsets.all(10),
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage("./assets/image/buu_logo_black.png"),
                    ),
                  ),
                )
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "มหาวิทยาลัยบูรพา",
                  style: TextStyle(fontSize: 20, color: Colors.black),
                ),
                Text(
                  "Burapha University",
                  style: TextStyle(fontSize: 15, color: Colors.black),
                ),
              ],
            ),
          ],
        )),
        backgroundColor: Colors.brown,
        elevation: 0.0,
        actions: <Widget>[
          badges.Badge(
            badgeAnimation: badges.BadgeAnimation.rotation(
              animationDuration: Duration(seconds: 1),
              colorChangeAnimationDuration: Duration(seconds: 1),
              loopAnimation: false,
              curve: Curves.fastOutSlowIn,
              colorChangeAnimationCurve: Curves.easeInCubic,
            ),
            position: badges.BadgePosition.custom(start: 10.0),
            child: IconButton(
              icon: Icon(Icons.notifications),
              iconSize: 35,
              color: Colors.black,
              onPressed: () {
                setState(() {
                });
              },
            ),
          ),
        ],
      ),
      body: ListView(
        children: [
          SpGrid(width: MediaQuery.of(context).size.width, children: [
            SpGridItem(
                xs: 12,
                sm: 12,
                md: 12,
                lg: 12,
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                  child: Center(
                    child: Container(

                      height: 50,
                      width: 500,

                    ),
                  ),
                )),
            SpGridItem(
              xs: 12,
              sm: 6,
              md: 4,
              lg: 4,
              child: Container(
                child: buildCard(
                    "ขอเชิญบัณฑิตมหาวิทยาลัยบูรพา ปีการศึกษา",
                    "2564 ตอบแบบสำรวจภาวะการมีงาน...",
                    "assets/image/pic1.png",
                    "ประกาศโดย: Burapha University",
                    context),
              ),
            ),
            SpGridItem(
              xs: 12,
              sm: 6,
              md: 4,
              lg: 4,
              child: Container(
                child: buildCard(
                    "การชำระค่าธรรมเนียมการศึกษา",
                    "ขอให้นิสิตทุกคนชำระค่าธรรมเนียมการศึกษาภายในวันที่ 22 ธันวาคม 2565",
                    "assets/image/pic2.png",
                    "ประกาศโดย: Burapha University",
                    context),
              ),
            ),
            SpGridItem(
              xs: 12,
              sm: 6,
              md: 4,
              lg: 4,
              child: Container(
                child: buildCard(
                    "ระเบียบสำหรับแนบเบิกค่าเล่าเรียนบุตร",
                    "สอบถามข้อมูลที่กองคลังฯ ชั้น 3 อาคารสำนักงานอธิการบดี(ภปร) โทร 038-102157",
                    "assets/image/pic3.png",
                    "ประกาศโดย: Burapha University",
                    context),
              ),
            ),
            SpGridItem(
              xs: 12,
              sm: 6,
              md: 4,
              lg: 4,
              child: Container(
                child: buildCard(
                    "มหาวิทยาลัยบูรพา จัดพิธีถวายพระพรชัยมงคล",
                    "แด่ เจ้าฟ้าพัชรกิติยาภาฯ",
                    "assets/image/pic4.png",
                    "ประกาศโดย: Burapha University",
                    context),
              ),
            ),
            SpGridItem(
              xs: 12,
              sm: 6,
              md: 4,
              lg: 4,
              child: Container(
                child: buildCard(
                    "มหาวิทยาลัยบูรพา เข้าร่วมพิธีถวายเครื่องราชสักการะ",
                    "เนื่องใน “วันสมเด็จพระเจ้าตากสินมหาราช” ประจำปี ๒๕๖๕",
                    "assets/image/pic5.png",
                    "ประกาศโดย: Burapha University",
                    context),
              ),
            ),
            SpGridItem(
              xs: 12,
              sm: 6,
              md: 4,
              lg: 4,
              child: Container(
                child: buildCard(
                    "มหาวิทยาลัยบูรพา เข้าร่วมพิธีถวายราชสักการะเนื่องใน",
                    "วันพ่อขุนรามคำแหงมหาราช ประจำปี ๒๕๖๖",
                    "assets/image/pic6.png",
                    "ประกาศโดย: Burapha University",
                    context),
              ),
            ),
          ]),
        ],
      ),
      drawer: MyDrawer(),
    );
  }

  Card buildCard(String head, String sub, String card, String support,
      BuildContext context) {
    var heading = head;
    var subheading = sub;
    var cardImage = AssetImage('$card');
    var supportingText = support;
    return Card(
        elevation: 4.0,
        child: Column(
          children: [
            ListTile(
              title: Text(heading),
              subtitle: Text(subheading),
            ),
            Container(
              child: AspectRatio(
                aspectRatio: 2 / 1,
                child: Container(
                  child: Ink.image(
                    image: cardImage,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(16.0),
              alignment: Alignment.centerLeft,
              child: Text(supportingText),
            ),
          ],
        ));
  }
}
